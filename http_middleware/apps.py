from django.apps import AppConfig


class HttpMiddlewareConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'http_middleware'
