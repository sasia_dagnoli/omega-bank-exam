from django.http import HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from .models import Ledger, Account
from django.core.exceptions import FieldError


class BankToBankTransfer(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(BankToBankTransfer, self).dispatch(request, *args, **kwargs)

    def post(self, request):

        token = request.POST['idempotency_token']

        if Ledger.objects.filter(transactionId=token).exists():
            print("transaction has already been send once before")
            return HttpResponse(status=400)
        else:
            credit_account = Account.objects.get(id=request.POST['credit_account'])
            amount = int(request.POST['amount'])
            debit_text = request.POST['debit_text']
            credit_text = request.POST['credit_text']
            debit_account = Account.objects.get(accountName='Omega Bank Account')
            try:
                Ledger.bank_to_bank_transfer(
                    idempotency_token=token,
                    amount=amount,
                    debit_account=debit_account,
                    debit_text=debit_text,
                    credit_account=credit_account,
                    credit_text=credit_text,
                )

                print(request.POST, 'transaction successful')
                return HttpResponse(status=200)
            except FieldError:
                print("Transaction failed - invalid data")
                return HttpResponse(status=400)
