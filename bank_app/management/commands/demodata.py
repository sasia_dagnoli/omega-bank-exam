from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from bank_app.models import Account, CustomUser, Rank, Ledger
import uuid


class Command(BaseCommand):
    def handle(self, **options):
        print('Adding demo data....')

        # The bank user and account
        bank_user = User.objects.create_user('bank', 'omega@bank.com', 'bankproject')
        bank_user.first_name = 'Omega'
        bank_user.last_name = 'Bank'
        bank_user.is_staff = True
        bank_user.is_superuser = True
        bank_user.save()
        bank_customuser = CustomUser.objects.create(user=bank_user, phone='12345678', rank=Rank.objects.get(value=30))
        bank_customuser.save()
        # create accounts
        credit_account = Account.objects.create(user=bank_user, accountName='Omega Bank Account')
        debit_account = Account.objects.create(user=bank_user, accountName='Omega Bank Loan', isLoan=True)
        # transfer money
        transaction_id = uuid.uuid4()
        Ledger.objects.create(transactionId=transaction_id, account=debit_account, amount=-100000, text='Demodata', isLoan=True)
        Ledger.objects.create(transactionId=transaction_id, account=credit_account, amount=100000, text='Demodata', isLoan=True)

        # admin user
        admin_user = User.objects.create_user('admin', 'admin@omegabank.com', 'bankproject')
        admin_user.first_name = 'Admine'
        admin_user.last_name = 'Adminsen'
        admin_user.is_staff = True
        admin_user.is_superuser = True
        admin_user.save()
        # add customuser to admin user
        admin_customuser = CustomUser.objects.create(user=admin_user, phone='1234544', rank=Rank.objects.get(value=20))
        admin_customuser.save()
        # create accounts
        bank_account = Account.objects.create(user=admin_user, accountName='Payroll Account')
        Account.objects.create(user=admin_user, accountName='Savings Account')
        loan_account = Account.objects.create(user=admin_user, accountName='Housing Loan', isLoan=True)
        # transfer money
        transaction_id = uuid.uuid4()
        Ledger.objects.create(transactionId=transaction_id, account=loan_account, amount=-25000, text='Demodata', isLoan=True)
        Ledger.objects.create(transactionId=transaction_id, account=bank_account, amount=25000, text='Demodata', isLoan=True)

        # custom user
        custom_user = User.objects.create_user('johndoe', 'john@doe.com', 'bankproject')
        custom_user.first_name = 'John'
        custom_user.last_name = 'Doe'
        custom_user.save()
        CustomUser.objects.create(user=custom_user, phone='12345678')
        Account.objects.create(user=custom_user, accountName='Budget Account')
