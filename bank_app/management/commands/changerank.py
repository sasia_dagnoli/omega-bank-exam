from django.core.management.base import BaseCommand
from bank_app.models import CustomUser, Rank, Account
from django.shortcuts import get_object_or_404


class Command(BaseCommand):
    def handle(self, **options):
        Account.objects.filter(isActive=True, isLoan=False)
        user = CustomUser.objects.all()
        for u in user:
            for a in u.accounts:
                balance = a.balance
                if balance >= 500000:
                    rank = get_object_or_404(Rank, name='Silver')
                    u.rank = rank
                    u.save(update_fields=['rank'])
                elif balance >= 1000000:
                    rank = get_object_or_404(Rank, name='Gold')
                    u.rank = rank
                    u.save(update_fields=['rank'])
